package com.luban.ziya.sync;

import org.openjdk.jol.info.ClassLayout;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

/**
 * Created By ziya
 * 2020/9/14
 */
public class Wait_1 {

    static Object obj = new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            synchronized (obj) {
                System.out.println(ClassLayout.parseInstance(obj).toPrintable());

                try {
                    obj.wait();
                    System.out.println("t1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println(ClassLayout.parseInstance(obj).toPrintable());
            }
        });

        Thread t3 = new Thread(() -> {
            synchronized (obj) {
                System.out.println(ClassLayout.parseInstance(obj).toPrintable());

                try {
                    obj.wait();
                    System.out.println("t3");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println(ClassLayout.parseInstance(obj).toPrintable());
            }
        });

        Thread t2 = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (obj) {
                obj.notify();
            }
        });

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();
    }
}
